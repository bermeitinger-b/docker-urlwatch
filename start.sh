#!/usr/bin/env bash
set -e

cmd=("urlwatch")

if [[ $# -ne 0 ]]; then
  cmd+=("$@")
fi

exec "${cmd[@]}"
