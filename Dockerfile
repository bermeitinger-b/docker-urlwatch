# syntax=docker/dockerfile:1.4
FROM python:3.13-slim AS base

SHELL ["/bin/bash", "-e", "-u", "-o", "pipefail", "-c"]

ARG USER_NAME="appuser"
ENV USER_NAME="${USER_NAME}"
ARG USER_ID="1000"
ENV USER_ID="${USER_ID}"
ENV USER_HOME="/home/${USER_NAME}"
ENV APP_ROOT="/app"
ENV PIP_CACHE="${USER_HOME}/.cache/pip"

ENV PYTHONUNBUFFERED="1"
ENV PYTHONDONTWRITEBYTECODE="1"
ENV PYTHONIOENCODING="UTF-8"

RUN \
  --mount=type=cache,sharing=locked,target=/var/cache/apt \
  --mount=type=cache,sharing=locked,target=/var/lib/apt \
  <<EOF

  # Setup
  export DEBIAN_FRONTEND="noninteractive"
  echo 'debconf debconf/frontend select Noninteractive' | debconf-set-selections
  rm -f /etc/apt/apt.conf.d/docker-clean
  echo 'Binary::apt::APT::Keep-Downloaded-Packages "true";' > /etc/apt/apt.conf.d/keep-cache

  # Install runtime dependencies
  apt-get update
  apt-get install --no-install-recommends -y \
    "gosu" \
    "lynx" \
    "libpoppler-cpp0v5" \
    "wdiff"

  # Create user
  useradd \
      --create-home \
      --no-log-init \
      --shell "/bin/bash" \
      --uid "${USER_ID}" \
      "${USER_NAME}"

  mkdir -p "${APP_ROOT}"
  mkdir -p "${PIP_CACHE}"

  chown -R "${USER_NAME}:${USER_NAME}" "${USER_HOME}"
  chown -R "${USER_NAME}:${USER_NAME}" "${APP_ROOT}"

  # Done.
EOF

FROM base as uv
RUN \
  --mount=type=cache,sharing=locked,target=/var/cache/apt \
  --mount=type=cache,sharing=locked,target=/var/lib/apt \
  <<EOF
  # Install basic dependencies
  apt-get update -q
  apt-get install --yes --no-install-recommends \
    "build-essential" \
    "curl" \
    "git" \
    "pkg-config" \
    "libpoppler-dev" \
    "libpoppler-cpp-dev"

  # Done.
EOF

ENV UV_CACHE_DIR="${USER_HOME}/.cache/uv"
ENV UV_COMPILE_BYTECODE=1
ENV UV_LINK_MODE=copy

# Install uv via their docker image
COPY --from=ghcr.io/astral-sh/uv:latest /uv /uvx /usr/local/bin/

WORKDIR "${APP_ROOT}"
USER "${USER_NAME}"
RUN \
  --mount=type=bind,source=uv.lock,target=uv.lock \
  --mount=type=bind,source=pyproject.toml,target=pyproject.toml \
  --mount=type=cache,uid="${USER_ID}",target="${UV_CACHE_DIR}" \
  <<EOF

  # Install dependencies and create venv
  uv sync --frozen --no-install-project

  # Done.
EOF

COPY --chown="${USER_NAME}" . "${APP_ROOT}"
RUN \
  --mount=type=cache,uid="${USER_ID}",target="${UV_CACHE_DIR}" \
  <<EOF

  # Install project
  uv sync --frozen

  # Done.
EOF


FROM base as final
COPY --from=uv "${APP_ROOT}" "${APP_ROOT}"

ENV PATH="${APP_ROOT}/.venv/bin:${PATH}"
RUN \
  --mount=type=cache,sharing=locked,target=/var/cache/apt \
  --mount=type=cache,sharing=locked,target=/var/lib/apt \
  <<EOF

  python -m playwright install-deps firefox

EOF


USER "${USER_NAME}"
RUN \
  <<EOF
  python -m playwright install firefox
EOF

COPY *.sh /usr/local/bin/
ENTRYPOINT ["/usr/local/bin/entrypoint.sh"]

USER root
