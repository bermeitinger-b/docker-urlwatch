#!/usr/bin/env bash
set -e

chown -R "${USER_NAME}:${USER_NAME}" "/home/${USER_NAME}"

if [[ $# -eq 0 ]]; then
  cmd=("bash")
else
  cmd=("$@")
fi

exec gosu "${USER_NAME}" "${cmd[@]}"
